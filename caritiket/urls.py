from django.urls import path,include

from . import views

app_name = 'caritiket'

urlpatterns = [
   path('viewjualtiket/', views.viewjualtiket, name='viewjualtiket'),
   path('isijualtiket/', views.isijualtiket, name='isijualtiket'),
]
