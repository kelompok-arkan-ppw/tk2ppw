from django import forms

from .models import Tiket

class tiketForm(forms.ModelForm):


    jenisTiket = forms.CharField(
    widget= forms.TextInput(
        attrs= {
            "class" : "txt-form form-control",
            "required"  : True,
            "placeholder"   : "Kategori",
            }
        )
    )


    tanggal = forms.DateField(
    widget = forms.SelectDateWidget(
        attrs = {
            "class" : "pilih-tanggal form-control ",
            "required"  : True,
            }
        )
    )

    nomorTelepon = forms.CharField(
    widget= forms.TextInput(
        attrs= {
            "class" : "txt-form form-control",
            "required"  : True,
            "placeholder"   : "Nomor Telepon",
            }
        )
    )


    class Meta:
        model = Tiket
        fields = {
            'namaKonser',
            'jenisTiket',
            'tempat',
            'tanggal',
            'namaPenjual',
            'nomorTelepon',
            'fotoTiket',
            'alasan',
        }
