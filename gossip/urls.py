from django.urls import path
from . import views

app_name = 'gossip'

urlpatterns = [
    path('', views.gossip, name='mygossip'),
    path('data/', views.data, name = 'mygossipdata')
]