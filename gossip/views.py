from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def data(request):
	try:
		q = request.GET['q']

	except:
		q = 'quilting'

	json_read = requests.get('https://newsapi.org/v2/everything?q=' + q + '&from=2019-11-13&sortBy=publishedAt&apiKey=cd1e7bae59c0402b8da9e2fc6be02bc4').json()
	return JsonResponse(json_read)


def gossip(request):
    return render(request, 'gossip.html')

