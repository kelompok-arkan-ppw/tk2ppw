from django.test import TestCase, Client
from django.urls import resolve
from gossip.views import *

# Create your tests here.
class TestGossip(TestCase):
    def test_gossip_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code,200)

    def test_gossip_using_html_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'gossip.html')

    def test_konser_using_func_konser_views(self):
        found = resolve('/news/')
        self.assertEqual(found.func, gossip)
