from django.db import models
from django.utils import timezone
# Create your models here.

class KoncoKonser(models.Model):
    namalengkap = models.CharField(max_length= 75)
    notelepon = models.CharField(max_length=20)
    namakonser = models.CharField(max_length= 75)
    tempatkonser = models.CharField(max_length= 75)
    tanggalkonser = models.DateField(default = timezone.now)
    pesan = models.CharField(max_length= 255)

    def __str__(self):
        return "{}".format(self.namalengkap)
