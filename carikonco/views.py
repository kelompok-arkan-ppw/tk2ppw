from django.shortcuts import render, redirect
from . import forms
from .models import KoncoKonser

# Create your views here.
def searchKonser(request):
    return render(request, 'searchKoncoKonser.html')

def findKonco(request):
    return render(request, 'findKonco.html')

def formFindKonco(request):
    form = forms.FormFindKonser()
    if request.method == "POST":
        form = forms.FormFindKonser(request.POST)
        if form.is_valid():
            form.save()
            return redirect('carikonco:myfindkonco')
    return render(request, 'formFindKonco.html', {'form':form})

def findKonco(request):
    daftar_item = KoncoKonser.objects.order_by('tanggalkonser')
    response = {
        'daftar_item' :daftar_item,
    }
    return render(request, 'findKonco.html', response)

def delete_konco(request, id):
    KoncoKonser.objects.get(id = id).delete()
    return redirect('carikonco:myfindkonco')




