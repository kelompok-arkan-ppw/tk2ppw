from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from carikonco.views import findKonco, formFindKonco, delete_konco
from .forms import FormFindKonser
from .models import KoncoKonser

# Create your tests here.
class TestFindKonco(TestCase):
    def test_findKonco_using_func_findKonco_views(self):
        found = resolve('/findKonco/myfindkonco/')
        self.assertEqual(found.func, findKonco)

    def test_myfindKonco_pake_base(self):
        response = Client().get('/findKonco/myfindkonco/')
        self.assertTemplateUsed(response, 'findKonco.html')

    def test_myfindKonco_ada_Find_Your_Konco_Here(self):
        response = Client().get('/findKonco/myfindkonco/')
        content = response.content.decode("utf8")
        self.assertIn("Find Your Konco Here!", content)


class TestFormKonco(TestCase):
    def test_formKonco_is_exist(self):
        response = Client().get('/findKonco/myformkonco/')
        self.assertEqual(response.status_code,200)

    def test_formKonco_using_html_template(self):
        response = Client().get('/findKonco/myformkonco/')
        self.assertTemplateUsed(response, 'formFindKonco.html')

    def test_formKonco_using_func_formFindKonco_views(self):
        found = resolve('/findKonco/myformkonco/')
        self.assertEqual(found.func, formFindKonco)

    def test_myformKonco_pake_base(self):
        response = Client().get('/findKonco/myformkonco/')
        self.assertTemplateUsed(response, 'base.html')

    def test_myFormKonco_ada_Add_Yourself_Here(self):
        response = Client().get('/findKonco/myformkonco/')
        content = response.content.decode("utf8")
        self.assertIn("Add Yourself Here!", content)

    def test_model_KoncoKonser_bisa_add_yourself(self):
        add_saya = KoncoKonser.objects.create(namalengkap="nama", 
                                                notelepon="0123456", 
                                                namakonser="konsercoba",
                                                tempatkonser="jakarta",
                                                tanggalkonser=timezone.now(),
                                                pesan="ini pesannn")
        hitungAdd = KoncoKonser.objects.all().count()
        self.assertEqual(hitungAdd, 1)
    """
    def test_input_form_FormFindKonser(self):
        response = Client().post('', data={'namalengkap':"nama",
                                            'notelepon':"0123456",
                                            'namakonser':"konsercoba",
                                            'tempatkonser':"jakarta", 
                                            'tanggalkonser':timezone.now(),
                                            'pesan':"ini pesannn"})
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn("nama", content)
    """
