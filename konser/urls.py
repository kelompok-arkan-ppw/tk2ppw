from django.urls import path
from . import views

app_name = 'konser'

urlpatterns = [
    path('', views.konser, name='mykonser'),
    path('formevents/', views.formkonser, name='myformkonser'),
]