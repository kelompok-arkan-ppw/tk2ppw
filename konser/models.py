from django.db import models
from django.utils import timezone
# Create your models here.

class EventKonser(models.Model):
    namakonser = models.CharField(max_length= 75)
    instagram = models.CharField(max_length=20)
    twitter = models.CharField(max_length= 75)
    tempatkonser = models.CharField(max_length= 75)
    tanggalkonser = models.DateField(default = timezone.now)
    poster = models.CharField(max_length = 2000)
    pesan = models.CharField(max_length= 50000)

    def __str__(self):
        return "{}".format(self.namakonser)
