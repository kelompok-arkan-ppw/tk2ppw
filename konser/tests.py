from django.test import TestCase, Client
from django.urls import resolve
from konser.views import *
from .forms import FormKonser

# Create your tests here.
class TestKonser(TestCase):
    def test_konser_is_exist(self):
        response = Client().get('/events/')
        self.assertEqual(response.status_code,200)

    def test_konser_using_html_template(self):
        response = Client().get('/events/')
        self.assertTemplateUsed(response, 'konser.html')