from django.urls import path, re_path
from .views import *

app_name = "users"

urlpatterns = [
    #re_path(r'^login/$', login_view, name="login"),
    re_path(r'^signup/$', signup_view, name = "signup"),
    re_path(r'^profile/$', profile, name="profile")
]