from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect, HttpResponse
from .forms import CustomUserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import user_logged_in
# Create your views here.
'''
def login_view(request):
  if request.method == 'POST':
    form = AuthenticationForm(data=request.POST)
    if form.is_valid():
      # log in the user
      return render(request, "homepage.html")
  else:
    form = AuthenticationForm()
  return render(request, 'registration/login.html', {'form': form})
'''

def signup_view(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            print(True)
            form.save()
        return redirect('login')
    else:
        form = CustomUserCreationForm()
    return render(request, 'signup.html', {'form': form})


def profile(request):
    return render(request, 'registration/profile.html')