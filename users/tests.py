from django.test import TestCase, Client, LiveServerTestCase
from .models import CustomUser

# Create your tests here.
class Concert_User_Test(TestCase):
    def test_landing_url(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "homepage.html")

    def test_signup_url(self):
        c = Client()
        response = c.get('/users/signup/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "signup.html")

    def test_login_url(self):
        c = Client()
        response = c.get('/users/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "registration/login.html")
    
    def test_signup(self):
        count = CustomUser.objects.all().count()
        self.assertEqual(count, 0)

        data = {
            'username':'kucing',
            'email':'kucing@kucing.com',
            'password1': 'kucinganjing1',
            'password2': 'kucinganjing1',
        }

        response = self.client.post('/users/signup/', data)
        count2 = CustomUser.objects.all().count()

        self.assertEqual(count2, 1)
        self.assertEqual(response.status_code, 302)


    def test_session_id_exist_when_logged_in(self):
        user = CustomUser.objects.create_user('myusername', 'myemail@crazymail.com', 'mypassword')
        user.save()
        response = self.client.post('/users/login/', data={'username':'myusername', 'password':'mypassword'})
        self.assertEqual(response.status_code, 302)
        self.assertIn('_auth_user_id', self.client.session)