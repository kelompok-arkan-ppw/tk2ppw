from django.urls import path

from .views import*

app_name = "homepage"

urlpatterns = [
    path('', Homepage.as_view(), name='Homepage'),
]